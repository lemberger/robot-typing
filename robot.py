# This file is part of robot-typing
# a small robot-themed speed-typing game.
# https://gitlab.com/lemberger/robot-typing
#
# SPDX-FileCopyrightText: 2021 Thomas Lemberger <https://thomaslemberger.com>
#
#!/usr/bin/env python3

from colorama import Fore, Back, Style, init
import random
import string
import time


def print_page(text=""):
    lines = text.split("\n")
    print("\033[F" * (len(lines) - 1))
    for line in lines:
        print(line, end="\n")


def matrix_animation(iterations: int, columns=80, lines=50, sleep=0.001):
    page = []
    for n in range(iterations):
        for m in range(lines):
            line = ""
            for i in range(columns):
                letter = "\n"
                while letter == "\n":
                    letter = random.choice(string.ascii_letters + string.digits)
                color = random.choice(
                    [
                        Fore.LIGHTBLACK_EX,
                        Fore.MAGENTA,
                        Fore.BLUE,
                        Fore.GREEN,
                        Fore.RED,
                        Fore.CYAN,
                    ]
                )
                line += color + letter + " "
            page.append(line)
            if len(page) > lines:
                page = page[-lines:]
            print_page("\n".join(page))
            time.sleep(sleep)
    print(Fore.RESET)


def white_noise(iterations: int, columns=160, lines=50, sleep=0.001):
    for n in range(iterations):
        page = []
        for m in range(lines):
            line = ""
            for i in range(columns):
                letter = "\n"
                while letter == "\n":
                    letter = random.choice(string.ascii_letters + string.digits)
                line += letter + ""
            page.append(line)
            if len(page) > lines:
                page = page[-lines:]
        print_page("\n".join(page))
        time.sleep(sleep)


def robot(columns=160, lines=50):
    def fill(line, before=20):
        before = " " * before
        after = columns - len(before) - len(line)
        return before + line + " " * after

    robot = [
        fill(line)
        for line in """
 _   _       _ _             _       _
| | | | __ _| | | ___     __| |_   _| |
| |_| |/ _` | | |/ _ \   / _` | | | | |
|  _  | (_| | | | (_) | | (_| | |_| |_|
|_| |_|\__,_|_|_|\___/   \__,_|\__,_(_)

                  ,--.    ,--.
                 ((O ))--((O ))
               ,'_`--'____`--'_`.                        
              _:  ____________  :_                           
             | | ||::::::::::|| | |          
             | | ||::::::::::|| | |          
             | | ||::::::::::|| | |          
             |_| |/__________\| |_|          
               |________________|            
            __..-'            `-..__         
         .-| : .----------------. : |-.      
       ,\ || | |\______________/| | || /.    
      /`.\:| | ||  __  __  __  || | |;/,'\            
     :`-._\;.| || '--''--''--' || |,:/_.-':           
     |    :  | || .----------. || |  :    |                          
     |    |  | || '----SSt---' || |  |    |                          
     |    |  | ||   _   _   _  || |  |    |                          
     :,--.;  | ||  (_) (_) (_) || |  :,--.;                          
     (`-'|)  | ||______________|| |  (|`-')                          
      `--'   | |/______________\| |   `--'                           
             |____________________|
              `.________________,'
               (_______)(_______)
               (_______)(_______)
               (_______)(_______)
               (_______)(_______)
              |        ||        |
              '--------''--------'
""".split(
            "\n"
        )
    ]
    lines_above = 10
    lines_below = lines - lines_above - len(robot)
    robot_lines = [" " * columns] * lines_above + robot

    def flicker(display, iterations=4):
        robot_filled = "\n".join(display + [" " * columns] * lines_below)
        for n in range(iterations):
            print_blank_page(lines=lines, columns=columns)
            time.sleep(0.1)
            print_page(robot_filled)
            time.sleep(0.3)

    flicker(robot_lines)
    print("\n".join(robot_lines))


def print_blank_page(lines=50, columns=160):
    blank_page = "\n".join([" " * columns] * lines)
    print_page(blank_page)


def print_indented(text):
    print(Fore.GREEN + text + Fore.RESET)


def print_countdown():
    print(Fore.RESET, end="")
    print("\rAuf die Plätze...   ", end="")
    time.sleep(1)
    print(Fore.YELLOW, end="")
    print("\rFertig...           ", end="")
    time.sleep(1)
    print(Fore.RED, end="")
    print("\rUnd Los!!!          ")
    print(Fore.RESET)
    print()


init()

while True:
    print(Fore.RESET)
    print("[ENTER drücken, um zu starten]", end="")
    input()
    print("\033[F                                           ")
    matrix_animation(2, sleep=0.03)
    white_noise(5, sleep=0.1)
    robot()
    print(
        "Tippe den folgenden Text so schnell du kannst, um meine Schaltkreise in Schwung zu bringen!"
    )
    print("Drücke die ENTER-Taste, um zu beginnen.")

    print_indented(
        """
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum!
    """
    )

    print("\r[ENTER drücken, um zu starten]", end="")
    input()
    print("\033[F                                           ")

    print_countdown()

    start_in_seconds = int(time.time())

    current_text = ""

    while not current_text.endswith("!"):
        current_text += input()

    end_in_seconds = int(time.time())

    print(Fore.GREEN)
    print(
        f"Glückwunsch! Du hast {end_in_seconds - start_in_seconds} Sekunden gebraucht!"
    )
    print(Fore.RESET)

    print("[ENTER drücken, um neu zu starten]")
    input()
    print_blank_page()
